package test

import (
	"context"
	"fmt"
	"gitee.com/King_of_Universe_Sailing/MircoCenter/ServiceCenter/apps/ServiceManage"
	"gitee.com/King_of_Universe_Sailing/MircoCenter/ServiceCenter/package/ServiceUtils"

	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"net"
	"testing"
)

type Ping struct {
	UnimplementedPingServer
}

func (p *Ping) Pong(context.Context, *emptypb.Empty) (*Reponse, error) {
	return &Reponse{Msg: "Pong"}, nil
}

func (p *Ping) StartServer(ProjectName, ServiceName, Ipaddr, Port string) {
	utils, err := ServiceUtils.NewServiceUtils()
	if err != nil {
		panic(err)
	}
	err = utils.RegisterService(context.Background(), &ServiceManage.Service{
		ProjectName: ProjectName,
		ServiceName: ServiceName,
		ServiceHost: fmt.Sprintf("%s:%s", Ipaddr, Port),
	})
	if err != nil {
		panic(err)
	}
	listen, err := net.Listen("tcp", fmt.Sprintf("%s:%s", Ipaddr, Port))
	if err != nil {
		panic(err)
	}
	server := grpc.NewServer()
	RegisterPingServer(server, p)
	err = server.Serve(listen)
	if err != nil {
		panic(err)
	}
}

func StartServer(ProjectName, ServiceName, Ipaddr, Port string) {
	var p Ping
	p.StartServer(ProjectName, ServiceName, Ipaddr, Port)
}

func TestStartServer(t *testing.T) {
	go StartServer("ServiceTest", "test", "127.0.0.1", "8081")
	go StartServer("ServiceTest", "test", "127.0.0.1", "8082")
	go StartServer("ServiceTest", "test", "127.0.0.1", "8083")
	go StartServer("ServiceTest", "test", "127.0.0.1", "8084")
	go StartServer("vblog", "login", "127.0.0.1", "8085")
	go StartServer("vblog", "login", "127.0.0.1", "8086")
	go StartServer("vblog", "login", "127.0.0.1", "8087")
	go StartServer("vblog", "login", "127.0.0.1", "8088")
	select {}
}
